import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import Principal from '@/views/Principal.vue'
import Veiculos from '@/views/Veiculos.vue'
import Apartamentos from '@/views/Apartamentos.vue'
import Pessoas from '@/views/Pessoas.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta:{
      auth: false,
      tittle: 'Login'
    }
  }, 
  {
    path: '/principal',
    name: 'principal',
    component: Principal,
    meta:{
      auth: true,
      tittle: 'Principal'
    }
  },
  {
    path: '/pessoas',
    name: 'pessoas',
    component: Pessoas,
    meta:{
      auth: true,
      tittle: 'Pessoas'
    }
  }, 
  {
    path: '/apartamentos',
    name: 'apartamentos',
    component: Apartamentos,
    meta:{
      auth: true,
      tittle: 'Apartamentos'
    }
  }, 
  {
    path: '/veiculos',
    name: 'veiculos',
    component: Veiculos,
    meta:{
      auth: true,
      tittle: 'Veiculos'
    }
  },    
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
