var servidor = "http://127.0.0.1:8090"
import cookies from './Cookies'
export function get(endereco){
  return new Promise((resolve,reject) =>{
    var request = new XMLHttpRequest();
    var token = cookies.getCookie("token"); 
    request.open("GET", servidor + "/api/" + endereco, true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization","bearer"+ token);    
    try{
      request.send();
      request.onreadystatechange = function(){
        if (request.readyState == request.DONE){
          if (request.status == 200){
            resolve(request.response);
          }
          reject(request.response);
        }
      };
    }catch(err){
      reject(err);
    }
  });
}

export function post(endereco, objeto){
  //console.log(endereco);
  return new Promise((resolve, reject)=>{
    var request = new XMLHttpRequest();    
    var json = JSON.stringify(objeto);        
    var token = cookies.getCookie("token");
    //if (protocolo == "http:"){
      request.open("POST", servidor + "/api/" + endereco, true);
      request.setRequestHeader("Content-Type","Application/json");
      request.setRequestHeader("Authorization","bearer"+ token);
    //}
    try{
      request.send(json);
      //console.log(json)
      request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
          if(request.status == 200){
            resolve(request.response);            
          } else{
            reject(request.response);
          }
        }
      }
    } catch(e){
      reject(e);
    }
  });
}

export function put(endereco, objeto){
  //console.log(endereco);
  return new Promise((resolve, reject)=>{
    var request = new XMLHttpRequest();    
    var json = JSON.stringify(objeto);        
    var token = cookies.getCookie("token");
    //if (protocolo == "http:"){
      request.open("PUT", servidor + "/api/" + endereco, true);
      request.setRequestHeader("Content-Type","Application/json");
      request.setRequestHeader("Authorization","bearer"+ token);
    //}
    try{
      request.send(json);
      //console.log(json)
      request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
          if(request.status == 200){
            resolve(request.response);            
          } else{
            reject(request.response);
          }
        }
      }
    } catch(e){
      reject(e);
    }
  });
}