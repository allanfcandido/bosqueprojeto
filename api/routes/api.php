<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::get('/', function(){
        return response()->json(['message' => 'Usuarios API', 'Status' => 'Connected']);;
    });
    //
    Route::post('auth/login','AutorizacaoController@login');
    Route::post('auth/logout','AutorizacaoController@logout');
    Route::post('auth', 'AutorizacaoController@ValidaToken');

    Route::post('Usuario', 'UsuarioController@store');

    //Tudo o que estiver dentro do grupo abaixo é protegido por token
    Route::group(['middleware' => ['jwt.verify']], function(){
        Route::get('atualizaacessos', 'PessoaController@atualizaacessos');
        Route::get('pessoaprop', 'PessoaController@pessoaXApartamentoXVeiculo');
        Route::post('authadm', 'AutorizacaoController@VerificaSeEAdministrador');
        Route::post('veiculo/altera/', 'VeiculoController@alteraVeiculo');
        Route::post('apartamento/altera/', 'ApartamentoController@alteraApartamento');
        Route::put('apartamento/desativa/','ApartamentoController@desativaApartamento');
        Route::put('usuario/desativa/', 'UsuarioController@desativaUsuario');
        Route::put('pessoa/desativa/','PessoaController@desativaPessoa');
        Route::put('veiculo/desativa/','VeiculoController@desativaVeiculo');
        Route::post('pessoa/altera','PessoaController@alteraPessoa');
        Route::resource('usuario', 'UsuarioController');
        Route::resource('apartamento', 'ApartamentoController');
        Route::resource('veiculo','VeiculoController');
        Route::resource('pessoa','PessoaController');
        Route::resource('evento','EventoController');
    });



Route::get('/', function(){
    return redirect('api');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
