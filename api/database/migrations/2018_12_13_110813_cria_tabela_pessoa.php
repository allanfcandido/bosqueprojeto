<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaPessoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa', function (Blueprint $table) {
            $table->bigIncrements('codpessoa');
            $table->string('observacao',500)->nullable();
            $table->integer('tipo')->nullable();
            //$table->integer('codpessoa')->unique();
            $table->string('contato',45)->nullable();
            $table->string('nome',150);
            $table->string('foto',200);
            $table->boolean('ativo')->default(true);
            $table->timestamps();
        });

        // DB::statement("
        // CREATE SEQUENCE public.sequenciador_codpessoa
        // INCREMENT 1
        // START 1
        // MINVALUE 1
        // MAXVALUE 9999999999999;");

        // DB::statement("ALTER TABLE public.pessoa
        // ALTER COLUMN codpessoa SET DEFAULT nextval('sequenciador_codpessoa'::regclass);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoa');
    }
}
