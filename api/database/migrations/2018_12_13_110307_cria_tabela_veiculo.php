<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaVeiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculo', function (Blueprint $table) {
            $table->bigIncrements('codveiculos');
            //$table->integer('codveiculo')->unique();
            $table->string('modelo',150);
            $table->string('cor',100);
            $table->string('placa',20)->unique();
            $table->boolean('ativo')->default(true);
            $table->timestamps();
        });

        // DB::statement("
        //     CREATE SEQUENCE public.sequenciador_codveiculo
        //     INCREMENT 1
        //     START 1
        //     MINVALUE 1
        //     MAXVALUE 9999999999999;");

        // DB::statement("ALTER TABLE public.veiculo
        // ALTER COLUMN codveiculo SET DEFAULT nextval('sequenciador_codveiculo'::regclass);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('veiculo', function (Blueprint $table) {
            //
        });
    }
}
