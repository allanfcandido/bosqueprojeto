<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codevento')->unique();
            $table->boolean('programado')->nullable();
            $table->date('dataprogramada')->nullable();;
            $table->time('horaprogramada')->nullable();;
            $table->date('datainicio')->nullable();;
            $table->time('horainicio')->nullable();;
            $table->date('datafim')->nullable();;
            $table->time('horafim')->nullable();;
            $table->string('observacao',1000)->nullable();;
            $table->string('sinistro',1000)->nullable();;
            $table->integer('codpessoa');
            $table->foreign('codpessoa')->references('codpessoa')->on('pessoa');
            $table->integer('codveiculo');
            $table->foreign('codveiculo')->references('codveiculo')->on('veiculo');
            $table->integer('codap');
            $table->foreign('codap')->references('codap')->on('apartamento');
            $table->timestamps();
        });
        DB::statement("
        CREATE SEQUENCE public.sequenciador_codevento
        INCREMENT 1
        START 1
        MINVALUE 1
        MAXVALUE 9999999999999;");

        DB::statement("ALTER TABLE public.evento
        ALTER COLUMN codevento SET DEFAULT nextval('sequenciador_codevento'::regclass);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento');
    }
}
