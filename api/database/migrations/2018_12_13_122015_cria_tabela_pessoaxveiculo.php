<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaPessoaxveiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoaxveiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codpessoa');
            $table->foreign('codpessoa')->references('codpessoa')->on('pessoa');
            $table->integer('codveiculo');
            $table->foreign('codveiculo')->references('codveiculo')->on('veiculo');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoaxveiculo');
    }
}
