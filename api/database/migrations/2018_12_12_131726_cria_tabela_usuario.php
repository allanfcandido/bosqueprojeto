<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario',100)->unique();
            $table->string('senha',350);
            $table->string('auth_token',400)->nullable();
            $table->boolean('ativo')->default(true);
            $table->boolean('superuser')->default(false);
            $table->timestamps();
        });

        //Cria a base já com um usuário administrador cadastrado
        $senha  = \Hash::make('alfa*184420'); //criptografa a senha
        DB::statement("
            insert into usuario(usuario,senha,ativo,superuser) values ('administrador','".$senha."',true,true);");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}

