<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaApartemento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartamento', function (Blueprint $table) {
            $table->bigIncrements('codap');
            $table->timestamps();
            $table->string('numero',15);
            $table->string('bloco',15);
            $table->boolean('ativo')->default(true);
        });

        // DB::statement("
        // CREATE SEQUENCE public.sequenciador_codap
        // INCREMENT 1
        // START 1
        // MINVALUE 1
        // MAXVALUE 9999999999999;");

        // DB::statement("ALTER TABLE public.apartamento
        // ALTER COLUMN codap SET DEFAULT nextval('sequenciador_codap'::regclass);");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartamento');
    }
}
