<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartamento extends Model
{
    protected $fillable  = ['numero','bloco','ativo','codap'];
    protected $hidden = ['updated_at','deleted_at','update_at','created_at'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $primaryKey ='codap';
    protected $table = 'apartamento';


    public function apartamento(){
        return $this->hasMany('App\PessoaXApartamento','codap','codap');
    }
}
