<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaXVeiculo extends Model
{
    protected $fillable  = ['codveiculo'];
    protected $hidden  = ['codpessoa','codveiculo','deleted_at','created_at','updated_at'];
    protected $table = 'pessoaxveiculo';

    public function veiculo(){
        return $this->belongsTo('App\veiculo', 'codveiculo', 'codveiculo');
    }
}
