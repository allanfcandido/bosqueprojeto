<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pessoa;
use App\PessoaXVeiculo;
use App\PessoaXApartamento;
use App\PessoaXAcesso;
use App\Acesso;
use App\Veiculo;
use App\Apartamento;
use Illuminate\Support\Facades\DB;
class PessoaController extends Controller
{
    public function index(){
        $pessoa = Pessoa::where('ativo','=','true')->orderBy('codpessoa','asc');
        return response()->json($pessoa);
    }

    //Esse metodo retorna um Json contendo informações sobre o usuario
    //e todos os veiculos e apartamentos que pertencem a ele
    public function pessoaXApartamentoXVeiculo(){
        $pessoaXApartamentoXVeiculo = Pessoa::with(['pessoaxveiculo.veiculo','pessoaxapartamento.apartamento'])->join('acesso','pessoa.tipo', '=', 'acesso.cod_acesso')->where('pessoa.ativo','=',true)->orderBy('codpessoa','asc')->get();
        return response()->json($pessoaXApartamentoXVeiculo);
    }

    //A função store tem duas funções nesse caso especifico.
    //Ao inserir uma nova pessoa, caso ela já exista, e o usuário esteja apenas colocando novos veículos ou apartamentos
    //Ele ira adicionar o veículo/apartamento a pessoa, caso a pessoa não esteja cadastrada, ele ira inseri-la no banco
    //Essa verificação é feita através do nome do usuário
    public function store(Request $request){
        $dados = $request->only(['nome','contato','observacao','tipo','ativo']);
        //error_log(print_r($dados, TRUE));
        $apartamentos = $request->apartamento;
        $veiculos = $request->veiculo;
        //$acesso = $request->tipo;
        //Verifica se a pessoa já existe
        $pessoa = Pessoa::where('nome',$dados['nome'])->first();
        error_log("Request:{$request}");
        //Se existir, apenas atualize as informações
		$numeroAleatorio = mt_rand(1,1000);
        if($pessoa){
            $nome = preg_replace('/\s+/', '', $dados['nome']);
            error_log("Foto:{$pessoa['foto']}");
            //if(strpos($request['image'], '.jpg') === false){
            if($request['image'] != $pessoa['foto']){
                $nomeArquivo = $numeroAleatorio.'_'.$pessoa->codpessoa.'_'.$nome.'.jpg';
                $caminhoPastaFotos = "..\\..\\site\\public\\img";    //"..\\..\\xampp\\htdocs\\img\\Pessoas\\"; ALLAN
                \Image::make(\file_get_contents($request->image))->save($caminhoPastaFotos."\\".$nomeArquivo)->orientate();
                $dados['foto'] = $nomeArquivo;
            }
            try{
                $pessoa = $pessoa->update($dados);
            } catch (Exception $e){
                error_log($e->getMessage());
            }
        //Se não, cadastre uma nova
        }else{
            $codpessoa = Pessoa::max("codpessoa");
            $codpessoa++;
            $nome = preg_replace('/\s+/', '', $dados['nome']);
            $nomeArquivo = $numeroAleatorio.'_'.$codpessoa.'_'.$nome.'.jpg';
            $caminhoPastaFotos = "..\\..\\site\\public\\img"; //"..\\..\\xampp\\htdocs\\img\\Pessoas\\"; ALLAN
            \Image::make(\file_get_contents($request->image))->save($caminhoPastaFotos."\\".$nomeArquivo)->orientate();
            $dados['foto'] = $nomeArquivo;
            $pessoa = Pessoa::create($dados);
        }
        //Em todos os casos, ele ira adicionar os veículos e apartamentos abaixo
        if($pessoa){
            //Procura pela pessoa no banco para retornar o codPessoa
            $pessoa = Pessoa::where('nome',$dados['nome'])->get();
            //Conta quantos veiculos foram cadastrados para essa pessoa
            $count = count($veiculos);
            //A linha a baixo, procura todas as ligações da pessoa com o veiculo e exclui e então, atualiza com os códigos corretos novamente
            //Sem essa linha, sempre que uma alteração fosse feita, ele duplicaria os registros
            DB::table('pessoaxveiculo')->where('codpessoa',$pessoa[0]->codpessoa)->delete();
            //Faz um for inserindo o codigo da pessoa e o codigo dos veiculos que pertencem a ela
            for($i = 0; $i < $count; $i++){
                DB::table('pessoaxveiculo')->insert([
                    ['codpessoa' => $pessoa[0]->codpessoa, 'codveiculo' => $veiculos[$i]]
                ]);
            }
            //A linha a baixo, procura todas as ligações da pessoa com o apartamento e exclui e então, atualiza com os códigos corretos novamente
            //Sem essa linha, sempre que uma alteração fosse feita, ele duplicaria os registros
            DB::table('pessoaxapartamento')->where('codpessoa',$pessoa[0]->codpessoa)->delete();
            //O Codigo a baixo faz a mesma coisa que o de cadastrar quantos apartamentos pertencem ao morador
            $count = count($apartamentos);
            for($i = 0; $i < $count; $i++){
                DB::table('pessoaxapartamento')->insert([
                    ['codpessoa' => $pessoa[0]->codpessoa, 'codap' => $apartamentos[$i]]
                ]);
            }
            return response()->json(['status'=>1]);
        }
        //Se tudo der certo, ele retornara um JSON com o status = 1 informando que esta tudo Ok
        else{
            //Senão, retornara um JSON com status 0 informando que algo deu errado
            return response()->json(['status'=>0]);
        }
    }

    public function alteraPessoa(Request $request){
        $dadosAlteracao = $request->except('image','apartamento','veiculos');
        $apartamentos = $request->apartamento;
        $veiculos = $request->veiculos;
        $acesso = $request->tipo;
        $pessoa = Pessoa::where('codpessoa',$dadosAlteracao['codpessoa'])->get(); //era 'id' onde esta cod pessoa ALLAN
		$numeroAleatorio = mt_rand(1,1000);
        if($request->image){
            $nome = preg_replace('/\s+/', '', $dadosAlteracao['nome']);
            $nomeArquivo = $numeroAleatorio.'_'.$pessoa[0]->codpessoa.'_'.$nome.'.jpg';
            $caminhoPastaFotos = ".\\";  //"..\\..\\xampp\\htdocs\\img\\Pessoas\\"; ALLAN
            \Image::make(\file_get_contents($request->image))->save($caminhoPastaFotos."\\".$nomeArquivo)->orientate();
            $dadosAlteracao['foto'] = $nomeArquivo;
        }

         //Procura pela pessoa no banco para retornar o codPessoa
         //Conta quantos veiculos foram cadastrados para essa pessoa
         $count = count($veiculos);
         //A linha a baixo, procura todas as ligações da pessoa com o veiculo e exclui e então, atualiza com os códigos corretos novamente
         //Sem essa linha, sempre que uma alteração fosse feita, ele duplicaria os registros
         DB::table('pessoaxveiculo')->where('codpessoa',$pessoa[0]->codpessoa)->delete();
         //Faz um for inserindo o codigo da pessoa e o codigo dos veiculos que pertencem a ela
         for($i = 0; $i < $count; $i++){
             DB::table('pessoaxveiculo')->insert([
                 ['codpessoa' => $pessoa[0]->codpessoa, 'codveiculo' => $veiculos[$i]]
             ]);
         }

         //A linha a baixo, procura todas as ligações da pessoa com o apartamento e exclui e então, atualiza com os códigos corretos novamente
         //Sem essa linha, sempre que uma alteração fosse feita, ele duplicaria os registros
         DB::table('pessoaxapartamento')->where('codpessoa',$pessoa[0]->codpessoa)->delete();
         //O Codigo a baixo faz a mesma coisa que o de cadastrar quantos apartamentos pertencem ao morador
         $count = count($apartamentos);
         for($i = 0; $i < $count; $i++){
             DB::table('pessoaxapartamento')->insert([
                 ['codpessoa' => $pessoa[0]->codpessoa, 'codap' => $apartamentos[$i]]
             ]);
        }


        try{
            $update = Pessoa::where('codpessoa','=',$pessoa[0]->codpessoa)->update($dadosAlteracao);

            if($update){
                return response()->json(['status'=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false, $e]);
        }
    }

    public function desativaPessoa(Request $request){
        $status = false;
        $dados = $request->all();
        $codpessoa = $dados['codpessoa'];
        try{
            $Pessoa = Pessoa::where('codpessoa', '=', $codpessoa);
            $desativa = $Pessoa->update($dados);
            if($desativa){
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }
}
