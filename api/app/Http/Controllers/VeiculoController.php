<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Veiculo;
class VeiculoController extends Controller
{
    public function index(){
        $veiculo = Veiculo::where('ativo','=',true)->get();
        return response()->json($veiculo);
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $veiculo = Veiculo::create($dados);
            if($veiculo){
                return response()->json(['status'=>1]);
            }else{
                return response()->json(['status'=>0]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0, $e]);
        }
    }

    public function desativaVeiculo(Request $request){
        $status = false;
        $dados = $request->all();
        $codveiculo = $dados['codveiculo'];
        try{
            $Veiculo = Veiculo::where('codveiculo', '=', $codveiculo);
            $desativa = $Veiculo->update($dados);
            if($desativa){
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }

    public function alteraVeiculo(Request $request){
        $dados = $request->all();
        try{
            $veiculo = Veiculo::where('codveiculo',$dados['codveiculo']);

            if($veiculo){
                $veiculo = $veiculo->update($dados);
                if($veiculo){
                    return response()->json(['status'=>true]);
                }
            }
            return response()->json(['status'=>false]);
        }catch(Exception $e){
            return response()->json(['status'=>false, 'erro'=> $e]);
        }
    }
}
