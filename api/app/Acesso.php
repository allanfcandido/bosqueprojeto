<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acesso extends Model
{
    protected $fillable  = ['tipo', 'cod_acesso'];
    //protected $hidden  = ['cod_acesso'];
    protected $primaryKey = 'cod_acesso';
    public $incrementing = false;
    protected $table = 'acesso';

    public function acesso(){
        return $this->hasMany('App\PessoaXAcesso','cod_acesso','cod_acesso');
    }
}
