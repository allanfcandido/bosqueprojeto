<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable  = ['usuario','superuser','auth_token','senha'];
    protected $hidden = ['senha','created_at','updated_at','deleted_at','update_at'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $table = 'usuario';

    public function usuario(){
        return $this->hasMany('App\Usuario');
    }


    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }
}
