<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaXApartamento extends Model
{
    protected $fillable  = ['codpessoa','codap'];
    protected $hidden  = ['codpessoa','codap'];
    protected $table = 'pessoaxapartamento';

    public function apartamento(){
        return $this->belongsTo('App\apartamento', 'codap', 'codap');
    }
}
