<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $fillable  = ['modelo','cor','placa','ativo', 'codveiculo'];
    protected $hidden = ['created_at','deleted_at','updated_at'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $primaryKey ='codveiculo';
    protected $table = 'veiculo';

    public function veiculo(){
        return $this->hasMany('App\Veiculo');
    }

}
