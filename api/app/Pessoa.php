<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable  = ['observacao','tipo','codpessoa','contato','nome','ativo','foto'];
    protected $hidden = ['created_at','deleted_at','updated_at'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $primaryKey ='codpessoa';
    protected $table = 'pessoa';

    public function pessoa(){
        return $this->hasMany('App\Pessoa');
    }

    public function pessoaxveiculo(){
        return $this->hasMany('App\pessoaxveiculo', 'codpessoa', 'codpessoa');
    }
    public function pessoaxapartamento(){
        return $this->hasMany('App\pessoaxapartamento', 'codpessoa', 'codpessoa');
    }
}
