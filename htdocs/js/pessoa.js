﻿var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;
async function registraPessoa(){
    await redimensionar();
    var Pessoa = new Object();
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var tipo = document.getElementById('slcTipoAcesso');
    Pessoa.nome = document.getElementById('txtNome').value;
    //retornaMultiplosSelecionados é um metodo que esta no arquivo global.js    
    Pessoa.apartamento = retornaMultiplosSelecionados('#slcApt ');
    Pessoa.veiculos = retornaMultiplosSelecionados('#slcVeiculo');
    Pessoa.telefone = document.getElementById('txtTelefone').value;
    Pessoa.celular = document.getElementById('txtCelular').value;
    Pessoa.observacao = document.getElementById('txtObs').value;
    Pessoa.tipo = tipo.options[tipo.selectedIndex].value;
    Pessoa.ativo = true;
    setTimeout(() => {
        Pessoa.image = document.getElementById('imgRedimensionada').src;
    }, 200);
    setTimeout(() => {
        JsonPessoa = JSON.stringify(Pessoa);
    }, 350);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/pessoa', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        setTimeout(() => {
            console.log(JsonPessoa);
            request.send(JsonPessoa);
        }, 550);
        request.onreadystatechange = function(){
            if(request.readyState == request.DONE){
                if(request.status == 200){
                    var NovaPessoa = new Object();
                    NovaPessoa = JSON.parse(request.response);                        
                    if(NovaPessoa.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Nova pessoa registrado com sucesso!";
                        setTimeout( function(){
                            location.reload();
                        },2000);

                    }else{                        
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";                        
                        if(NovaPessoa[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }                    
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
    }
}
function alteraPessoa(codPessoa){
    redimensionar();
    var Pessoa = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var tipo = document.getElementById('slcTipoAcesso');
    var JsonPessoa;
    Pessoa.nome = document.getElementById('txtNome').value;
    Pessoa.celular = document.getElementById('txtCelular').value;
    Pessoa.telefone = document.getElementById('txtTelefone').value;
    Pessoa.observacao = document.getElementById('txtObs').value;
    Pessoa.apartamento = retornaMultiplosSelecionados('#slcApt ');
    Pessoa.veiculos = retornaMultiplosSelecionados('#slcVeiculo');
    Pessoa.tipo = tipo.options[tipo.selectedIndex].value;
    Pessoa.id = codPessoa;
    setTimeout(() => {
        Pessoa.image  = document.getElementById('imgRedimensionada').src;
    }, 200);
    setTimeout(() => {
        JsonPessoa = JSON.stringify(Pessoa);
        console.log(JsonPessoa);
    }, 350);
    var token = document.cookie;
	//console.log(JsonPessoa);
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/pessoa/altera', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        setTimeout(() => {
            request.send(JsonPessoa);
        }, 500);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var PessoaAlterada = new Object();
                    PessoaAlterada = JSON.parse(request.response);
					console.log(request.response);
                    if(PessoaAlterada.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Pessoa alterada com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(PessoaAlterada[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "A pessoa informada já esta em uso!";
                        }
                        if(PessoaAlterada[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuário\nDescrição do erro.: '+err);
    }
}
function DesativaPessoa(codPessoa){
    var Pessoa = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErroModalDesativacao");
    var divTextoErro = document.getElementById("divTextoErroModalDesativacao");
    var JsonPessoa;
    Pessoa.codpessoa = codPessoa;
    Pessoa.ativo = false;
    JsonPessoa = JSON.stringify(Pessoa);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('PUT', url+':8090/api/pessoa/desativa/', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        request.responseType = "text";
        request.send(JsonPessoa);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var PessoaDesativada = new Object();
                    PessoaDesativada = JSON.parse(request.response);
                    if(PessoaDesativada.status != false){
                        divErro.className = "d-flex col-12 justify-content-center ";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-success text-center" ;
                        divTextoErro.innerHTML = "Pessoa desativada com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },3500);
                    }else{
                        alert('Não foi possivel desativar o usuario selecionado\nPor favor verifique o console do navegador');
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao desativar pessoa \nDescrição do erro.: '+err);
    }
}


function listaPessoas(ativaBotoesEdicao){
    var corpoTabelaPessoa = document.getElementById('corpoTabela');
    request = new XMLHttpRequest();
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('GET',url+':8090/api/pessoaprop', true);
    request.setRequestHeader("Authorization", "bearer "+token);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                var Pessoas = new Object();
                Pessoas = JSON.parse(request.response);
                //for que varre todas as pessoas do Json
                for(var pessoa in Pessoas){
                    //linha são as linhas da tabela normais
                    var linha = document.createElement('tr');
                    //linha aps serve para colocar os apartamentos numa unica celula da tabela
                    var tabelaAps = document.createElement('table');
                    tabelaAps.className = "table table-borderless m-0 p-0";
                    //Tive que fazer a mesma coisa para os veiculos
                    /*var tabelaVeiculos = document.createElement('table');
                    tabelaVeiculos.className = "table table-borderless m-0 p-0";
                    tabelaVeiculos.id = "corpoTabelaVeiculos";*/
                    
                    
                    //Insere o nome da codigo da pessoa na tabela
                    var codPessoa = document.createElement('td');
                    //codPessoa.className = "coluna-id-pessoa";
                    var texto = document.createTextNode(Pessoas[pessoa].codpessoa)
                    codPessoa.appendChild(texto);
                    codPessoa.className = "tamanhoColunaId";

                    //Insere o nome da pessoa na tabela
                    var nomePessoa = document.createElement('td');
                    nomePessoa.className = "coluna-nome-pessoa";
                    if(Pessoas[pessoa].observacao != null){
                        var texto = document.createTextNode(Pessoas[pessoa].nome+'\n\n\nObservações:\n'+Pessoas[pessoa].observacao);
                        var pre = document.createElement("pre");
                        pre.appendChild(texto);
                        pre.className = "tamanho-coluna-nome tamanho-fonte-tabela";
                        nomePessoa.appendChild(pre);
                    }else{
                        var texto = document.createTextNode(Pessoas[pessoa].nome);
                        nomePessoa.appendChild(texto);
                    }
                    nomePessoa.className = "tamanho-fonte-tabela tamanho-coluna-nome"
                    var tdFoto = document.createElement("td");
                    tdFoto.id = "tdFoto"+pessoa;
                    if(Pessoas[pessoa].foto != null){
                        var foto = document.createElement("img");
                        foto.src = "img/Pessoas/"+Pessoas[pessoa].foto;
                        foto.className = "tamanhoFotoTabela";
                        foto.linha = pessoa;
                        if(ativaBotoesEdicao){
                            foto.onclick = function(event) {
                                document.getElementById('btnAlterar'+this.linha).click();
                                corpoTabelaPessoa.focus();
                            }
                        }
                        tdFoto.appendChild(foto);
                        tdFoto.className = "tamanho-td-foto";
                    }
                    

                    //Esse for varre todos os apartamentos pertencentes a pessoa
                    var apartamentosSelecionados = [];
                    var qtdApartamentos = 0;
                    for(var apartamento in Pessoas[pessoa].pessoaxapartamento){
                        if(Pessoas[pessoa].pessoaxapartamento[apartamento].apartamento.ativo != false){
                            var linhaAps = document.createElement('tr');
                            apartamentosSelecionados[qtdApartamentos] = Pessoas[pessoa].pessoaxapartamento[apartamento].apartamento.codap;
                            //Para fazer com que ficassem tudo em uma unica celula
                            //foi preciso criar uma tabela dentro da celula apartamento
                            var ap = document.createElement('td');
                            //ap.className = "m-0 p-0 ";
                            var textNode = document.createTextNode(Pessoas[pessoa].pessoaxapartamento[apartamento].apartamento.numero+'-'+Pessoas[pessoa].pessoaxapartamento[apartamento].apartamento.bloco);
                            ap.appendChild(textNode);
                            ap.className = "tamanho-fonte-tabela padding-margin-0";
                            linhaAps.appendChild(ap);
                            tabelaAps.appendChild(linhaAps);
                            tabelaAps.style = "width:75px";
                            tabelaAps.className += " padding-margin-0"
                            qtdApartamentos++;
                        }
                    }
                    //A variavel tdAp é a celula que ira receber a outra tabela
                    //no for anterior, é preenchido a tabela de apartamentos, e aqui embaixo
                    //ele insere a tabela dentro da celular da outra tabela
                    //o mesmo acontece com os veiculos
                    var tdAp = document.createElement('td');
                    tdAp.className = "tamanho-td-ap";
                    tdAp.appendChild(tabelaAps);

                    //Esse for varre todos os veiculos pertencentes a pessoa
                    var veiculosPessoa = document.createElement('td');
                    var veiculosSelecionados = [];
                    var qtdVeiculos = 0;
					for(var veiculo in Pessoas[pessoa].pessoaxveiculo){
                        if(Pessoas[pessoa].pessoaxveiculo[veiculo].veiculo.ativo != false){
                            veiculosSelecionados[qtdVeiculos] = Pessoas[pessoa].pessoaxveiculo[veiculo].veiculo.codveiculo;
                            var texto = document.createTextNode(Pessoas[pessoa].pessoaxveiculo[veiculo].veiculo.placa+" - "+Pessoas[pessoa].pessoaxveiculo[veiculo].veiculo.modelo+" - "+Pessoas[pessoa].pessoaxveiculo[veiculo].veiculo.cor+'\n');
                            var pre = document.createElement("pre");
                            pre.appendChild(texto);
                            pre.className = "tamanho-fonte-tabela tamanho-td-veiculo";
                            veiculosPessoa.appendChild(pre);
                            veiculosPessoa.className = "tamanho-td-veiculo";
                            qtdVeiculos++;
                        }
                    }
                    
                    //Define o tipo de Pessoa
                    var tipoPessoa = Pessoas[pessoa].tipo;
                    switch(tipoPessoa){
                        case 1:
                            tipoPessoa = "Morador(a)";
                            break;
                        case 2:
                            tipoPessoa = "Visitante";
                            break;
                        case 3:
                            tipoPessoa = "Funcionário(a)";
                            break;
                        case 4:
                            tipoPessoa = "Prest. Serviço";
                            break;
                        default:
                            tipoPessoa = 'Sem acesso selecionado';
                            break;
                    }
                    var tdTipoPessoa = document.createElement('td');
                    var tipoPessoaTextNode = document.createTextNode(tipoPessoa);
                    tdTipoPessoa.appendChild(tipoPessoaTextNode);
                    tdTipoPessoa.className = "tamanho-td-acesso";

                    //O bloco de codigo a baixo serve para "perfumar"
                    //a coluna de telefone / celular
                    var telefoneCelular = document.createElement('td');
                    var celular = Pessoas[pessoa].celular;
                    var telefone = Pessoas[pessoa].telefone;
                    var contato;
                    if(telefone != null && celular == null){
                        contato = telefone;
                    }
                    if(telefone == null && celular != null){
                        contato = celular;
                    }
                    if(telefone != null && celular != null){
                        contato = telefone+ ' / '+ celular;
                    }
                    
                    if(telefone == null && celular == null){
                        contato = "Sem telefone/celular"
                    }
                    var textoTelefoneCelular = document.createTextNode(contato);
                    telefoneCelular.appendChild(textoTelefoneCelular);                    
                    telefoneCelular.className = "tamanho-td-contato";
                    /*
                    *   Cria dinamicamente botão de alterar com suas propriedas na tabela
                    */
                    if(ativaBotoesEdicao){
                        var SpanIconAlterar = document.createElement("span");
                        var BtnAlterar = document.createElement("button");
                        BtnAlterar.className = "btn btn-info m-0";
                        BtnAlterar.id = "btnAlterar"+pessoa;
                        SpanIconAlterar.className = "fa fa-edit";
                        BtnAlterar.appendChild(SpanIconAlterar);
                        BtnAlterar.title = "Alterar pessoa";
                        BtnAlterar.setAttribute("data-toggle","modal");
                        BtnAlterar.setAttribute("data-target","#modalPessoa");
                        BtnAlterar.setAttribute("role","button");
                        BtnAlterar.setAttribute("role","dialog");
                        BtnAlterar.pessoa = Pessoas[pessoa].nome;
                        BtnAlterar.codPessoa = Pessoas[pessoa].codpessoa;
                        BtnAlterar.telefone = Pessoas[pessoa].telefone;
                        BtnAlterar.celular = Pessoas[pessoa].celular;
                        BtnAlterar.obs = Pessoas[pessoa].observacao;
                        BtnAlterar.celular = Pessoas[pessoa].celular;
                        BtnAlterar.tipo = Pessoas[pessoa].tipo;
                        BtnAlterar.veiculos = veiculosSelecionados;
                        BtnAlterar.apartamentos = apartamentosSelecionados;
                        BtnAlterar.foto = "img/Pessoas/"+Pessoas[pessoa].foto;
                        BtnAlterar.onclick = function(){
                            abreTelaAlteracao(this);
                        }
                        /*
                        *   Cria dinamicamente botão de desativar com suas propriedas na tabela
                        */    
                        var SpanIconDesativar = document.createElement("span");
                        var BtnDesativar = document.createElement("button");
                        BtnDesativar.className = "btn btn-danger ml-1";
                        BtnDesativar.id = "btnDesativar"+pessoa;
                        SpanIconDesativar.className = "fa fa-trash";
                        BtnDesativar.appendChild(SpanIconDesativar);
                        BtnDesativar.title = "Desativar pessoa";
                        BtnDesativar.codpessoa = Pessoas[pessoa].codpessoa;
                        BtnDesativar.setAttribute("data-toggle","modal");
                        BtnDesativar.setAttribute("data-target","#modalDesativacaoPessoa");
                        BtnDesativar.setAttribute("role","button");
                        BtnDesativar.setAttribute("role","dialog");
                        var btnDesativaUsuarioModal = document.getElementById('btnDesativaPessoaModal');
                        BtnDesativar.onclick = function(){
                            btnDesativaUsuarioModal.codpessoa = this.codpessoa;
                        }
                    }//else{
                        /*
                        *   Cria dinamicamente os botões de registro e entrada 
                        */
                        /*var btnRegistraEntrada = document.createElement('button');
                        var iconeRegistraEntrada = document.createElement('img');
                        //Adiciona as classes aos botões
                        btnRegistraEntrada.className = "btn btn-primary ml-1 mt-1 mb-0";
                        //Adiciona o icone aos botões
                        iconeRegistraEntrada.src = "img/icones/Dados de entrada/icons8-dados-de-entrada-26.png";
                        //Adiciona os icones aos botões
                        btnRegistraEntrada.appendChild(iconeRegistraEntrada);
                        //personaliza os botões
                        btnRegistraEntrada.id = "btnRegistraEntrada"+pessoa;
                        btnRegistraEntrada.title = "Registrar entrada";
                        btnRegistraEntrada.setAttribute("data-toggle","modal");
                        btnRegistraEntrada.setAttribute("data-target","#modalPessoa");
                        btnRegistraEntrada.setAttribute("role","button");
                        btnRegistraEntrada.setAttribute("role","dialog");

                        btnRegistraEntrada.pessoa = Pessoas[pessoa].nome;
                        btnRegistraEntrada.codpessoa = Pessoas[pessoa].codpessoa;
                        btnRegistraEntrada.telefone = Pessoas[pessoa].telefone;
                        btnRegistraEntrada.celular = Pessoas[pessoa].celular;
                        btnRegistraEntrada.obs = Pessoas[pessoa].observacao;
                        btnRegistraEntrada.celular = Pessoas[pessoa].celular;
                        btnRegistraEntrada.tipo = Pessoas[pessoa].tipo;
                        btnRegistraEntrada.veiculos = veiculosSelecionados;
                        btnRegistraEntrada.apartamentos = apartamentosSelecionados;

                        btnRegistraEntrada.onclick = function(){
                            preparaTelaRegistroDeEntrada(this);
                        }
                }*/
                
                    //todos os linha.appendChild estão colocando
                    //os dados dentro da linha da coluna
                    //eles devem ser colocados na ordem correta
                    linha.appendChild(codPessoa);
                    linha.appendChild(tdFoto);
                    linha.appendChild(nomePessoa);
                    linha.appendChild(tdAp);
                    linha.appendChild(veiculosPessoa);
                    linha.appendChild(tdTipoPessoa)
                    linha.appendChild(telefoneCelular);
                    
                    if(ativaBotoesEdicao){
                        var tdAcoes = document.createElement("td");
                        tdAcoes.appendChild(BtnAlterar);
                        tdAcoes.appendChild(BtnDesativar);
                        //tdAcoes.className = "largura-acoes tamanho-td-acoes";
                        linha.appendChild(tdAcoes);
                    }else{
                        //linha.appendChild(btnRegistraEntrada);
                    }
                    linha.id="tr"+pessoa;
                    //Aqui, ele insere a linha na tabela, e o for da Pessoa finaliza
                    //Então ele cria uma nova linha
                    corpoTabelaPessoa.appendChild(linha);
                }
            }
        }
    }
    
}


function PreparaFormCadastroPessoa(){
    var btnSalvar = document.getElementById('btnSalvar');
    var tituloModal = document.getElementById('tituloModal');
    var txtObs = document.getElementById('txtObs');
    txtObs.innerText = "";
    document.getElementById("formPessoa").reset();
    
    tituloModal.innerHTML = 'Nova pessoa';
    btnSalvar.innerHTML = '';
    btnSalvar.innerHTML = '<img src="img/icones/SalvarEFechar/icons8-salvar-e-fechar-36.png"> Salvar';
    btnSalvar.onclick = function () {
        registraPessoa();
    }    
}

async function abreTelaAlteracao(dadosPessoaAlteracao){
	var Pessoa = new Object();
        Pessoa = dadosPessoaAlteracao;
        var btnSalvar = document.getElementById('btnSalvar');
        var tituloModal = document.getElementById('tituloModal');
        var listaVeiculosAlteracao = document.querySelector('#slcVeiculo');
        var listaApartamentosAlteracao = document.querySelector('#slcApt')
        var preview = document.getElementById('preview');
        preview.src = Pessoa.foto;
        await listaApartamentos(true);
        preview.className = 'img-fluid';
        //Preenche o modal de alteração com os dados da pessoa
        txtNome.value = Pessoa.pessoa;
        txtTelefone.value = Pessoa.telefone;
        txtObs.value = Pessoa.obs;
        txtCelular.value = Pessoa.celular;
        slcTipoAcesso.selectedIndex = Pessoa.tipo;
        btnSalvar.innerHTML = '';
        btnSalvar.innerHTML = '<img src="img/icones/SalvarEFechar/icons8-salvar-e-fechar-36.png"> Salvar';
        tituloModal.innerHTML = 'Alterar pessoa';
        btnSalvar.codPessoa = Pessoa.codPessoa;
        btnSalvar.onclick = function(){
            alteraPessoa(this.codPessoa);
        }
        var apartamentos = [];
        var veiculos = [];
        //https://stackoverflow.com/questions/12889552/how-to-set-selected-value-in-multi-value-select-in-jquery-select2
        setTimeout(() => {
            for(i = 0; i < Pessoa.apartamentos.length; i++){
                for(x = 0; x < listaApartamentosAlteracao.length; x++){
                    if(listaApartamentosAlteracao[x].value == Pessoa.apartamentos[i]){
                        apartamentos[i] = parseInt(listaApartamentosAlteracao[x].value);
                    }
                }
            }
            $(".selectApartamento").select2().val(apartamentos).trigger('change');
            console.log('selecionou os apartamentos!');
        }, 1500);
        
        console.log(Pessoa.veiculos);
        setTimeout(() => {
            for(i = 0; i < Pessoa.veiculos.length; i++){
                for(x = 0; x < listaVeiculosAlteracao.length; x++){
                    if(listaVeiculosAlteracao[x].value == Pessoa.veiculos[i]){
                        veiculos[i] = parseInt(listaVeiculosAlteracao[x].value);
                    }
                }
            }
            $(".selectVeiculo").select2().val(veiculos).trigger('change');
            console.log('selecionou os veiculos');
        }, 2000);
}

function preparaTelaRegistroDeEntrada(pessoaEntrando){
    document.getElementById("slcVeiculo").innerHTML = '';
    listaApartamentos(true,false);
    var PessoaEntrando = pessoaEntrando;
    document.getElementById('txtNome').disabled = true;
    document.getElementById('txtTelefone').disabled = true;
    document.getElementById('txtCelular').disabled = true;
    document.getElementById('txtNome').value = PessoaEntrando.pessoa;
    document.getElementById('txtTelefone').value = PessoaEntrando.telefone;
    document.getElementById('txtCelular').value = PessoaEntrando.celular;
    var btnRegistraEntrada = document.getElementById('btnSalvar');
    var listaVeiculoEntrada = document.querySelector('#slcVeiculo');
    var listaApartamentoEntrada = document.querySelector('#slcApt');
    //Precisei colocar um setTimeout (Uma especie de Timer) para poder executar a função
    //abaixo, por que se não, ele rodava o for antes mesmo de ter terminado de preencher
    //a lista de apartamentos, a mesma coisa acontece com a lista de veículos.
    //Essa função serve para já deixar selecionado os veículos/apartamentos que são
    //do cliente.
    //Apartamentos
    var existeApartamento;
    setTimeout(function () {
        for(i = 0; i < listaApartamentoEntrada.options.length; i++){
            existeApartamento = false;
            for(x = 0; x < PessoaEntrando.apartamentos.length ; x++){
                if(listaApartamentoEntrada[i].value == PessoaEntrando.apartamentos[x]){                 
                    existeApartamento = true;
                }
            }
            if(existeApartamento == false){
                listaApartamentoEntrada[i]= null;
                i--;
            }
        }
    },1000);
    //Veículos
    var existeVeiculo;
    setTimeout(function () {
        for(i = 0; i < listaVeiculoEntrada.options.length; i++){
            existeVeiculo = false;
            for(x = 0; x < PessoaEntrando.veiculos.length ; x++){
                if(listaVeiculoEntrada[i].value == PessoaEntrando.veiculos[x]){
                    existeVeiculo = true;
                }
            }
            if(existeVeiculo == false){
                listaVeiculoEntrada[i]= null;
                i--;
            }
        }
    },1000);
    btnRegistraEntrada.codpessoa = PessoaEntrando.codpessoa;    
    btnRegistraEntrada.onclick = function(){
        novoEvento(this.codpessoa);
    }
}