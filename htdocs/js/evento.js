var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;
var token = document.cookie;
token = token.replace("tk=","");

function novoEvento(codpessoa){
    try{
        var request = new XMLHttpRequest();
        var dataAtual = new Date();
        divErro = document.getElementById('divErro')
        divTextoErro = document.getElementById('divTextoErro');
        slcApt = document.getElementById('slcApt');
        slcVeiculo = document.getElementById('slcVeiculo');
        var PessoaEvento = new Object();        
        PessoaEvento.codpessoa = codpessoa;
        PessoaEvento.codveiculo = slcVeiculo.options[slcVeiculo.selectedIndex].value;
        PessoaEvento.codap = slcApt.options[slcApt.selectedIndex].value;
        PessoaEvento.datainicio = dataAtual.getDate()+'-'+(dataAtual.getMonth()+1)+'-'+dataAtual.getFullYear();
        PessoaEvento.horainicio = dataAtual.getHours()+':'+dataAtual.getMinutes()+':'+dataAtual.getSeconds();
        PessoaEvento.observacao = document.getElementById('txtObs').value;
        var JsonPessoaEvento = JSON.stringify(PessoaEvento);        
        request.open('POST',url+':8090/api/evento/', true);    
        request.setRequestHeader("Content-type","Application/json");
        request.setRequestHeader("Authorization","bearer "+token);    
        request.send(JsonPessoaEvento);
        request.onreadystatechange = function () {
            if(request.readyState == request.DONE){
                if(request.status == 200){
                    var StatusEvento = new Object();
                    StatusEvento = JSON.parse(request.response); 
                    if(StatusEvento.status == 1){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Novo evento registrado com sucesso!";
                        setTimeout(function(){
                            $('#modalPessoa').modal('hide');
                        },2000);
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-danger text-center";
                        divTextoErro.innerHTML = "Falha ao registrar novo evento!";
                    }
                }
            }
        }
        }catch(erro){
            console.log('Erro -> '+erro)
        }
        
}

function listaEventos(){    
    var corpoTabela = document.getElementById('corpoTabela');
    request = new XMLHttpRequest();
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('GET',url+':8090/api/evento', true);
    request.setRequestHeader("Authorization", "bearer "+token);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                var Evento = new Object();
                Evento = JSON.parse(request.response);
                for(var eventos in Evento){
                    var linha = document.createElement("tr");

                    var codevento = document.createElement("td");
                    var texto = document.createTextNode(Evento[eventos].codevento);
                    codevento.appendChild(texto);

                    var pessoa = document.createElement("td");
                    var texto = document.createTextNode(Evento[eventos].pessoa.nome);
                    pessoa.appendChild(texto);

                    var veiculo = document.createElement("td");
                    var texto = document.createTextNode(Evento[eventos].veiculo.placa+' - '+Evento[eventos].veiculo.modelo+' - '+Evento[eventos].veiculo.cor);
                    veiculo.appendChild(texto);

                    var ap = document.createElement("td");
                    var texto = document.createTextNode(Evento[eventos].apartamento.numero+ '-'+Evento[eventos].apartamento.bloco);
                    ap.appendChild(texto);


                    //Formata a hora para o formato DD/MM/YYYY
                    var data = Evento[eventos].datainicio;
                    data = data.replace('-','/');
                    data = data.replace('-','/');
                    var dataFormatada = new Date(data);
                    var dia = dataFormatada.getDate();
                    var mes = dataFormatada.getMonth() +1;
                    var ano = dataFormatada.getFullYear();
                    if(dia.toString.length == 1){
                        dia = '0'+dia;
                    }
                    if(mes.toString.length == 1){
                        mes = '0'+mes;
                    }
                    var dataHoraInicio = document.createElement("td");
                    var texto = document.createTextNode(dia+'/'+mes+'/'+ano+ ' ' +Evento[eventos].horainicio);
                    dataHoraInicio.appendChild(texto);

                    var spanIconVisualizar = document.createElement("span");
                    spanIconVisualizar.className = "fa fa-eye"
                    var btnVisualizar = document.createElement("button");
                    btnVisualizar.appendChild(spanIconVisualizar);
                    btnVisualizar.className = "btn btn-primary m-1";


                    var spanIconAlterar = document.createElement("span");
                    var btnAlterar = document.createElement("button");
                    btnAlterar.className = "btn btn-info m-1";
                    btnAlterar.id = 'btnAlterar'+eventos;
                    spanIconAlterar.className = "fa fa-edit";
                    btnAlterar.appendChild(spanIconAlterar);
                    btnAlterar.title = "Alterar evento";
                    btnAlterar.setAttribute("data-toggle","modal");
                    btnAlterar.setAttribute("data-target","#modalEvento");
                    btnAlterar.setAttribute("role","button");
                    btnAlterar.setAttribute("role","dialog");
                    var DadosEvento = new Object();
                    DadosEvento.codevento = Evento[eventos].codevento;
                    DadosEvento.modelo = Evento[eventos].veiculo.modelo;
                    DadosEvento.cor = Evento[eventos].veiculo.cor;
                    DadosEvento.placa = Evento[eventos].veiculo.placa;
                    DadosEvento.numero = Evento[eventos].apartamento.numero;
                    DadosEvento.bloco = Evento[eventos].apartamento.bloco;
                    btnAlterar.DadosEvento = DadosEvento;
                    btnAlterar.onclick = function(){                        
                        var btnSalvar = document.getElementById('btnSalvar');
                        var tituloModal = document.getElementById('tituloModal');
                        tituloModal.innerHTML = 'Alterar Evento';
                        btnSalvar.innerHTML = 'Alterar';
                        btnSalvar.codveiculo = this.codveiculo;
                        btnSalvar.onclick = function(){
                            alteraVeiculo(this.codveiculo);
                        }
                    }
                    /*
                    *   Cria dinamicamente botão de desativar com suas propriedas na tabela
                    */                    

                    var spanIconDesativar = document.createElement("span");
                    var btnDesativar = document.createElement("button");
                    btnDesativar.className = "btn btn-danger m-1 ";
                    //btnDesativar.id = "btnDesativar"+veiculo;
                    spanIconDesativar.className = "fa fa-trash";
                    btnDesativar.appendChild(spanIconDesativar);
                    btnDesativar.title = "Desativar veiculo";
                    //btnDesativar.codveiculo = Veiculos[veiculo].codveiculo;
                    btnDesativar.setAttribute("data-toggle","modal");
                    btnDesativar.setAttribute("data-target","#modalDesativacaoVeiculo");
                    btnDesativar.setAttribute("role","button");
                    btnDesativar.setAttribute("role","dialog");
                    var btnDesativaVeiculoModal = document.getElementById('btnDesativaVeiculo');
                    btnDesativar.onclick = function(){
                        //btnDesativaVeiculoModal.codveiculo = this.codveiculo;
                    }

                    linha.appendChild(codevento);
                    linha.appendChild(pessoa);
                    linha.appendChild(veiculo);
                    linha.appendChild(ap);
                    linha.appendChild(dataHoraInicio);
                    linha.appendChild(btnVisualizar);
                    linha.appendChild(btnAlterar);
                    linha.appendChild(btnDesativar);
                    corpoTabela.appendChild(linha);
                }
            }
            
        }
    }
}
