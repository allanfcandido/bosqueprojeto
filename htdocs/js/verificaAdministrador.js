function VerificaAdministrador(){   
    var url = location.host;
    var protocolo = location.protocol;
    ;url = url.replace(":5500","");;
    url = protocolo+"//"+url; 
    var Administrador = new Object();
    var Token = new Object();
    var request = new XMLHttpRequest();
    var tokenCookie = document.cookie;    
    Token.token = tokenCookie.replace('tk=','');    
    var JsonToken = JSON.stringify(Token);
    var dropDownMenuAdm = document.getElementById("listaAdmDropDown");
    //Verifica se o usuario é administrador ou não para poder liberar os menus de administrador
    try{
        request.open('POST', url+':8090/api/authadm', true);
        request.setRequestHeader("Content-type", "application/json");        
        request.send(JsonToken);
        request.onreadystatechange = function(){
            if(request.readyState == request.DONE){
                if(request.status == 200){               
                    Administrador = JSON.parse(request.response);                    
                    if(Administrador[0].superuser){
                        dropDownMenuAdm.className = "nav-item dropdown exibeMenuAdministrador";
                    }else{                        
                        dropDownMenuAdm.className = 'ocultaMenuAdministrador';
                    }
                }
            }
        }
    }catch(err){
        console.log("Erro ->"+err)
    }
}