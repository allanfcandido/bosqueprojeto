var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;
function RegistraUsuario(){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var administrador = document.getElementById('chkAdministrador');
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonUsuario;
    Usuario.usuario = document.getElementById('txtUsuario').value;    
    Usuario.senha = document.getElementById('txtSenha').value;
    Usuario.superuser = false;
    if(Usuario.senha == null || Usuario.senha == ""){
        divErro.className = "d-flex col-12 justify-content-center";
        divTextoErro.className = "col-6 alert alert-danger text-center";
        divTextoErro.innerHTML = "Por favor, preencha todos os campos";
    }else{
        if(administrador.checked){
            Usuario.superuser = true;
        }
        Usuario.ativo = true;
        JsonUsuario = JSON.stringify(Usuario);
        var token = document.cookie;
        token = token.replace("tk=", "");
        request.open('POST', url+':8090/api/usuario', true);
        request.setRequestHeader("Content-Type", "Application/json");
        request.setRequestHeader("Authorization", "bearer "+token);
        try{
            request.send(JsonUsuario);
            request.onreadystatechange = function(){
                if(request.readyState = request.DONE){
                    if(request.status == 200){
                        var NovoUsuario = new Object();
                        NovoUsuario = JSON.parse(request.response);
                        if(NovoUsuario.status != false){
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                            divTextoErro.innerHTML = "Novo usuário registrado com sucesso!";
                            setTimeout( function(){
                                location.reload();
                            },2000);

                        }else{
                            divErro.className = "d-flex col-12 justify-content-center";
                            divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                            if(NovoUsuario[0].errorInfo[0] == 23505){
                                divTextoErro.innerHTML = "O usuário informado já esta em uso!";
                            }
                            if(NovoUsuario[0].errorInfo[0] == 23502){
                                divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                            }
                        }
                    }
                }
            }
        }catch(err){
            console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
        }
        }
    }
    
function AlteraUsuario(idUsuario){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var chkAdministrador = document.getElementById('chkAdministrador');
    var divErro = document.getElementById("divErro");
    var divTextoErro = document.getElementById("divTextoErro");
    var JsonUsuario;
    var id = idUsuario;   
    Usuario.nome = document.getElementById('txtUsuario').value;          
    Usuario.senha = document.getElementById('txtSenha').value;

    if(document.getElementById('txtUsuario').value == null || document.getElementById('txtUsuario').value == ""){
        Usuario.usuario = undefined;
    }    
    if(document.getElementById('txtSenha').value == null || document.getElementById('txtSenha').value == ""){
        Usuario.senha = undefined;
    }
    Usuario.superuser = false;
    if(chkAdministrador.checked){
        Usuario.superuser = true;
    }
    JsonUsuario = JSON.stringify(Usuario);
    var token = document.cookie;
    token = token.replace("tk=", "");    
    request.open('PUT', url+':8090/api/usuario/'+id, true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);    
    try{
        request.send(JsonUsuario);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var UsuarioAlterado = new Object();
                    UsuarioAlterado = JSON.parse(request.response);
                    if(UsuarioAlterado.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Usuario alterado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(UsuarioAlterado[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "O usuario informado já esta em uso!";
                        }
                        if(UsuarioAlterado[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
    }
}



function DesativaUsuario(idUsuario){
    var Usuario = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErroModal");
    var divTextoErro = document.getElementById("divTextoErroModal");
    var JsonUsuario;
    Usuario.id = idUsuario;
    Usuario.ativo = false;
    JsonUsuario = JSON.stringify(Usuario);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('PUT', url+':8090/api/usuario/desativa', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);    
    try{
        request.send(JsonUsuario);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var UsuarioDesativado = new Object();
                    UsuarioDesativado = JSON.parse(request.response);
                    if(UsuarioDesativado.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-success text-center";        
                        divTextoErro.innerHTML = "Usuário desativado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },2500);                        
                    }else{                        
                        console.log(UsuarioDesativado);
                        alert('Não foi possivel desativar o usuario selecionado\nPor favor verifique o console do navegador');
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
    }
}


function listaUsuarios(){
    var CorpoTabelaUsuarios = document.getElementById('corpoTabela');
    request = new XMLHttpRequest();
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('GET',url+':8090/api/usuario', true);
    request.setRequestHeader("Authorization", "bearer "+token);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                var Usuarios = new Object();                
                Usuarios = JSON.parse(request.response);                
                for(var usuario in Usuarios){
                    //Insere ID Usuario na tabela
                    var linha = document.createElement('tr');

                    var idUsuario = document.createElement('td');
                    var texto = document.createTextNode(Usuarios[usuario].id)
                    idUsuario.appendChild(texto);

                    var nomeUsuario = document.createElement('td');
                    var texto = document.createTextNode(Usuarios[usuario].usuario)
                    nomeUsuario.appendChild(texto);

                    /*
                    *   Cria dinamicamente botão de alterar com suas propriedas na tabela
                    */
                    var SpanIconAlterar = document.createElement("span");
                    var BtnAlterar = document.createElement("button");
                    BtnAlterar.className = "btn btn-info m-1 ";
                    BtnAlterar.id = "btnAlterar"+linha;
                    BtnAlterar.line = linha;
                    SpanIconAlterar.className = "fa fa-edit";
                    BtnAlterar.appendChild(SpanIconAlterar);
                    BtnAlterar.title = "Alterar produto";
                    BtnAlterar.setAttribute("data-toggle","modal");
                    BtnAlterar.setAttribute("data-target","#modalUsuario");
                    BtnAlterar.setAttribute("role","button");
                    BtnAlterar.setAttribute("role","dialog");                                
                    BtnAlterar.superuser = Usuarios[usuario].superuser;                    
                    BtnAlterar.usuario = Usuarios[usuario].usuario;
                    BtnAlterar.idUsuario = Usuarios[usuario].id;

                    BtnAlterar.onclick = function(){
                        var btnSalvar = document.getElementById('btnSalvar');
                        var tituloModal = document.getElementById('tituloModal');
                        var chkAdministrador = document.getElementById('chkAdministrador');
                        chkAdministrador.checked = this.superuser;                                                
                        txtUsuario.value = this.usuario;
                        tituloModal.innerHTML = 'Alterar usuario';
                        btnSalvar.innerHTML = 'Alterar';
                        btnSalvar.idUsuario = this.idUsuario;
                        btnSalvar.onclick = function(){
                            AlteraUsuario(this.idUsuario);
                        }
                    }
                    /*
                    *   Cria dinamicamente botão de desativar com suas propriedas na tabela
                    */
                    
                    var SpanIconDesativar = document.createElement("span");
                    var BtnDesativar = document.createElement("button");
                    BtnDesativar.className = "btn btn-danger m-1 ";                    
                    BtnDesativar.id = "btnDesativar"+linha;
                    BtnDesativar.line = linha;
                    SpanIconDesativar.className = "fa fa-trash";
                    BtnDesativar.appendChild(SpanIconDesativar);
                    BtnDesativar.title = "Desativar produto";
                    BtnDesativar.idUsuario = Usuarios[usuario].id;
                    BtnDesativar.setAttribute("data-toggle","modal");
                    BtnDesativar.setAttribute("data-target","#modalDesativacaoUsuario");
                    BtnDesativar.setAttribute("role","button");
                    BtnDesativar.setAttribute("role","dialog");
                    var btnDesativaUsuarioModal = document.getElementById('btnDesativaUsuarioModal');
                    BtnDesativar.onclick = function(){
                        btnDesativaUsuarioModal.idUsuario = this.idUsuario;
                    }
                    linha.appendChild(idUsuario);
                    linha.appendChild(nomeUsuario);
                    linha.appendChild(BtnAlterar);
                    linha.appendChild(BtnDesativar);
                    CorpoTabelaUsuarios.appendChild(linha);

                }
            }
        }
    }
    
}

function PreparaFormCadastro(){
    var btnSalvar = document.getElementById('btnSalvar');
    var tituloModal = document.getElementById('tituloModal');
    tituloModal.innerHTML = 'Novo usuario';
    btnSalvar.innerHTML = 'Salvar';
}

//Login OK
function FazLogin(){    
    var UsuarioLogando = new Object();
    var UsuarioLogado = new Object();
    var request = new XMLHttpRequest();
    var caixaErro = document.getElementById('divCaixaErro');
    var usuario = document.getElementById("txtUsuario").value.toLowerCase();
    UsuarioLogando.usuario = usuario;
    UsuarioLogando.senha = document.getElementById("txtSenha").value;
    if(UsuarioLogando.usuario == null || UsuarioLogando.usuario == "" || UsuarioLogando.senha == null || UsuarioLogando.senha == ""){
        caixaErro.className = "alert alert-warning mensagemErro text-center mt-1 mb-1";
        caixaErro.innerHTML = "Por favor, informe seu usuário e senha";
        caixaErro.role = 'alert';
    }else{
        console.log(url);
        var JsonLogin = JSON.stringify(UsuarioLogando);
        console.log(url+':8090/api/auth/login/');
        request.open('POST', url+':8090/api/auth/login/',true);
        request.setRequestHeader("Content-type","application/json");
        try{            
            request.send(JsonLogin);
            request.onreadystatechange = function(){
            if(request.readyState == XMLHttpRequest.DONE){
                if(request.status == 200){
                    delete_cookie('tk');
                    caixaErro.className = "alert alert-success mensagemErro text-center mt-1 mb-1";
                    caixaErro.innerHTML = "Sucesso!";
                    caixaErro.role = 'alert';                
                    UsuarioLogado = JSON.parse(request.response);
                    document.cookie += "tk="+UsuarioLogado.token_acesso;
                    window.location.href = "principal.html";
                }else{
                    var Erro = JSON.parse(request.response);
                    console.log(Erro);
                    caixaErro.className = "alert alert-danger mensagemErro text-center mt-1 mb-1";
                    caixaErro.role = 'alert';
                    caixaErro.innerHTML = Erro.Erro;
                }
            }
        }
        }catch(err){
            console.log("Erro: "+err)
        }
    }
    
}

function delete_cookie(name) {    
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function KeyPressEnterLogin(event){
    if(event.keyCode == 13){
        FazLogin();
    }
}
function SetaProximoFoco(event,proximoElemento){ 
    if(event.keyCode == 13){
        document.getElementById(proximoElemento).focus();   
    }
}

function sleep(ms){
    return new Promisse(resolve => setTimeout(resolve, ms));
}