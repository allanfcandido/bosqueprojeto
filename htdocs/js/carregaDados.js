var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;



function CarregaTabela(){        
    var request = new XMLHttpRequest();
    var tokenCookie = document.cookie;    
    tokenCookie = tokenCookie.replace('tk=','');
    var corpoTabela = document.getElementById("corpoTabela"); 
    request.open('GET', url+':8090/api/historico/'+tokenCookie, true);
    request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("Authorization", "bearer "+tokenCookie);  
    request.send();    
    request.onreadystatechange = function(){
        try{
            if(request.readyState == XMLHttpRequest.DONE){
                if(request.status == 200){
                    dadosPainel = JSON.parse(request.response);                        
                    try{
                        for(var linha in dadosPainel){ 
                            //Cria o elemento de nova linha na tabela                            
                            var LinhaTabela = document.createElement("tr");
                            //Adiciona campo data na tabela
                            var dataFormatada = dadosPainel[linha].data;
                            dataFormatada = dataFormatada.replace('-','/');
                            dataFormatada = dataFormatada.replace('-','/');
                            var data = new Date(dataFormatada);                            
                            var dia = data.getDate();
                            var mes = data.getMonth() +1;
                            var ano = data.getFullYear(); 
                            if(dia.toString().length == 1){
                                dia = '0'+dia;
                            }
                            if(mes.toString().length == 1){
                                mes = '0'+mes;
                            }
                            data = dia+'/'+mes+'/'+ano;
                            var texto = document.createTextNode(data);
                            var data = document.createElement("td");
                            data.appendChild(texto);

                            //Adiciona campo hora na tabela
                            var hora = document.createElement("td");
                            var texto = document.createTextNode(dadosPainel[linha].hora);
                            hora.appendChild(texto);
                            LinhaTabela.appendChild(data);
                            LinhaTabela.appendChild(hora);
                            corpoTabela.appendChild(LinhaTabela);
                        }                           
                        
                    }catch(err){
                        console.log('Erro ao inserir dados no dropdown '+err);
                    }
                    
                }else{
                    console.log("Response: "+request.response+"\nCodigo: "+request.status);
                }
            }
        }catch(err){
            console.log('erro detectado = '+err)
        }
    }
}
function CarregaContadores(){
    var request = new XMLHttpRequest();
    var tokenCookie = document.cookie;    
    tokenCookie = tokenCookie.replace('tk=','');
    var dia,mes,semana,total;
    dia = document.getElementById('contadorHoje');
    semana = document.getElementById('contadorSemanal');
    mes = document.getElementById('contadorMensal');
    total = document.getElementById('contadorTotal');
    request.open('GET', url+':8090/api/totais/'+tokenCookie,true);
    request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("Authorization", "bearer "+tokenCookie);    
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){                
                contadores = JSON.parse(request.response);      
                var texto = document.createTextNode(contadores.hoje);
                dia.appendChild(texto);
                var texto = document.createTextNode(contadores.mes);
                mes.appendChild(texto);
                var texto = document.createTextNode(contadores.semana);
                semana.appendChild(texto);
                var texto = document.createTextNode(contadores.total);
                total.appendChild(texto);
                
            }
        }
    }
}