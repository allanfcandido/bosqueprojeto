function redimensionar(){
    var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var imageObj = new Image();
      var larguraMaxima = 1920;
      var alturaMaxima = 1080;
      var src = "Teste";
      imageObj.onload = function() {
        if(imageObj.width > larguraMaxima){
          imageObj.width = larguraMaxima;
          canvas.width = larguraMaxima;
        }else{
          canvas.width = imageObj.width;
        }
        if(imageObj.height > alturaMaxima){
          imageObj.height = alturaMaxima;
          canvas.height = alturaMaxima;
        }else{
          canvas.height = imageObj.height;
        }
        context.drawImage(imageObj, 0, 0,canvas.width,canvas.height);
        //Os parametros do toDataURL são o tipo de arquivo, e a qualidade onde 0.1 é ruim e 1.0 a melhor qualidade
        document.getElementById('imgRedimensionada').src = canvas.toDataURL('image/jpeg',0.4);
        //src = canvas.toDataURL('image/jpeg',0.5);
        //document.getElementById('uploadGaleria').value = canvas.toDataURL('image/jpeg',0.1);
        //console.log(canvas.toDataURL('image/jpeg',0.5));
      };
      imageObj.src = document.getElementById('preview').src;    
}


