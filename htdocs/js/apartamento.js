var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;


function RegistraApartamento(inserindoPorFora){    
    var Apartamento = new Object();
    var request = new XMLHttpRequest();    
    var divErro = document.getElementById("divErroAp");
    var divTextoErro = document.getElementById("divTextoErroAp");
    var JsonApartamento;
    Apartamento.numero = document.getElementById('txtNumAp').value;
    Apartamento.bloco = document.getElementById('txtBloco').value;
    Apartamento.ativo = true;
    JsonApartamento = JSON.stringify(Apartamento);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/apartamento', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        request.send(JsonApartamento);        
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var NovoApartamento = new Object();
                    NovoApartamento = JSON.parse(request.response);
                    if(NovoApartamento.status != 0){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Novo apartamento registrado com sucesso!";
                        //Esse inserido por fora funciona da seguinte maneira
                        //Caso o apartamento esteja sendo inserido durante um cadastro
                        //Ele deve enviar true, com isso, quando o cadastro estiver finalizado
                        //Ele fechara o modal de cadastro de apartamento e atualizará a lista
                        //Caso seja false, ele ira atualizar a pagina completamente
                        if(inserindoPorFora){
                            listaApartamentos();
                            setTimeout(function(){
                                $('#modalApartamento').modal('hide');
                            },2000);
                        }else{
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        }
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(NovoApartamento[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "O usuário informado já esta em uso!";
                        }
                        if(NovoApartamento[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo AP\nDescrição do erro.: '+err);
    }
}

function listaApartamentos(listaVeiculosJunto,listaTabela){
    return new Promise((resolve,reject) =>{
        if(listaTabela){
            var corpoTabelaApartamento = document.getElementById('corpoTabela');
            corpoTabelaApartamento.innerHTML = "";
        }else{
            document.getElementById("slcApt").innerHTML = "";
            var selectApartamentos = document.getElementById('slcApt');
        }
        request = new XMLHttpRequest();
        var token = document.cookie;
        token = token.replace("tk=", "");
        request.open('GET',url+':8090/api/apartamento', true);
        request.setRequestHeader("Authorization", "bearer "+token);
        request.send();
        request.onreadystatechange = function(){
            if(request.readyState == XMLHttpRequest.DONE){
                if(request.status == 200){
                    var Apartamentos = new Object();
                    Apartamentos = JSON.parse(request.response);
                    if(!listaTabela){
                        for(var ap in Apartamentos){
                            //Insere ID Usuario na tabela
                            var option = document.createElement('option');
                            var texto = document.createTextNode(Apartamentos[ap].numero+'-'+Apartamentos[ap].bloco);
                            option.value = Apartamentos[ap].codap;
                            option.appendChild(texto);
                            selectApartamentos.appendChild(option);
                        }
                        if(listaVeiculosJunto){
                            listaVeiculos(false);
                        }
                    }else{
                        for(var apartamento in Apartamentos){
                            var linha = document.createElement("tr");
                            var codap = document.createElement("td");
                            var texto = document.createTextNode(Apartamentos[apartamento].codap);
                            codap.appendChild(texto);
    
                            var numeroEBloco = document.createElement("td");
                            var texto = document.createTextNode(Apartamentos[apartamento].numero+'-'+Apartamentos[apartamento].bloco);
                            numeroEBloco.appendChild(texto);
    
                            var spanIconAlterar = document.createElement("span");
                            var btnAlterar = document.createElement("button");
                            btnAlterar.className = "btn btn-info m-0";
                            btnAlterar.id = 'btnAlterar'+apartamento;
                            spanIconAlterar.className = "fa fa-edit";
                            btnAlterar.appendChild(spanIconAlterar);
                            btnAlterar.title = "Alterar apartamento";
                            btnAlterar.setAttribute("data-toggle","modal");
                            btnAlterar.setAttribute("data-target","#modalApartamento");
                            btnAlterar.setAttribute("role","button");
                            btnAlterar.setAttribute("role","dialog");
                            btnAlterar.codap = Apartamentos[apartamento].codap;
                            btnAlterar.numero = Apartamentos[apartamento].numero;
                            btnAlterar.bloco = Apartamentos[apartamento].bloco;
        
                            btnAlterar.onclick = function(){
                                var btnSalvar = document.getElementById('btnSalvar');
                                var tituloModal = document.getElementById('tituloModal');                            
                                txtNumAp.value = this.numero;
                                txtBloco.value = this.bloco;
                                tituloModal.innerHTML = 'Alterar apartamento';
                                btnSalvar.innerHTML = '';
                                btnSalvar.innerHTML = '<img src="img/icones/SalvarEFechar/icons8-salvar-e-fechar-36.png"> Salvar';
                                btnSalvar.codap = this.codap;
                                btnSalvar.onclick = function(){
                                    alteraApartamento(this.codap);                                
                                }
                            }
                            /*
                            *   Cria dinamicamente botão de desativar com suas propriedas na tabela
                            */                    
        
                            var spanIconDesativar = document.createElement("span");
                            var btnDesativar = document.createElement("button");
                            btnDesativar.className = "btn btn-danger ml-1 ";
                            btnDesativar.id = "btnDesativar"+apartamento;
                            spanIconDesativar.className = "fa fa-trash";
                            btnDesativar.appendChild(spanIconDesativar);
                            btnDesativar.title = "Desativar apartamento";
                            btnDesativar.codap = Apartamentos[apartamento].codap;
                            btnDesativar.setAttribute("data-toggle","modal");
                            btnDesativar.setAttribute("data-target","#modalDesativacaoApartamento");
                            btnDesativar.setAttribute("role","button");
                            btnDesativar.setAttribute("role","dialog");
                            var btnDesativaApartamento = document.getElementById('btnDesativarApartamento');
                            btnDesativar.onclick = function(){                            
                                btnDesativaApartamento.codap = this.codap;
                            }
                            var tdAcoes = document.createElement("td");
                            tdAcoes.appendChild(btnAlterar);
                            tdAcoes.appendChild(btnDesativar);
                            //tdAcoes.className = "largura-acoes";
                            linha.appendChild(codap);
                            linha.appendChild(numeroEBloco);
                            linha.appendChild(tdAcoes);
                            corpoTabelaApartamento.appendChild(linha);
                        }
                    }
                    resolve('terminou a requisição');
                }
            }
        }    
    });
    
}

function desativaApartamento(codApartamento){
    
    var Apartamento = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErroModalDesativacao");
    var divTextoErro = document.getElementById("divTextoErroModalDesativacao");
    var JsonApartamento;
    Apartamento.codap = codApartamento;
    Apartamento.ativo = false;
    JsonApartamento = JSON.stringify(Apartamento);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('PUT', url+':8090/api/apartamento/desativa/', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{        
        
        request.responseType = "text";
        request.send(JsonApartamento);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var ApartamentoDesativado = new Object();
                    ApartamentoDesativado = JSON.parse(request.response);
                    if(ApartamentoDesativado.status != false){
                        divErro.className = "d-flex col-12 justify-content-center ";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-success text-center" ;
                        divTextoErro.innerHTML = "Apartamento desativado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },3500);
                    }else{
                        console.log(ApartamentoDesativado);
                        divErro.className = "d-flex col-12 justify-content-center ";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center" ;
                        divTextoErro.innerHTML = "Não foi possivel desativar o apartamento selecionado<br>Por favor verifique o console do navegador'";
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao desativar apartamento\nDescrição do erro.: '+err);
    }
}

function alteraApartamento(codap){
    var Apartamento = new Object();
    var request = new XMLHttpRequest();    
    var divErro = document.getElementById("divErroAp");
    var divTextoErro = document.getElementById("divTextoErroAp");
    var JsonApartamento;
    Apartamento.numero = document.getElementById('txtNumAp').value;
    Apartamento.bloco = document.getElementById('txtBloco').value;    
    Apartamento.codap = codap;
    JsonApartamento = JSON.stringify(Apartamento);    
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/apartamento/altera/', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        request.responseText = "text";
        request.send(JsonApartamento);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var apartamentoAlterado = new Object();
                    apartamentoAlterado = JSON.parse(request.response);                    
                    if(apartamentoAlterado.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "apartamento alterado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(apartamentoAlterado[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "O apartamento informado já esta cadastrado!";
                        }
                        if(apartamentoAlterado[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
    }
}
