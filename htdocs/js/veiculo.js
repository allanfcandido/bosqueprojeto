var url = location.host;
var protocolo = location.protocol;
;url = url.replace(":5500","");;
url = protocolo+"//"+url;


function registraVeiculo(inserindoPorFora){
    var Veiculo = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErroVeiculo");
    var divTextoErro = document.getElementById("divTextoErroVeiculo");
    var JsonVeiculo;
    Veiculo.modelo = document.getElementById('txtModelo').value.toUpperCase();
    Veiculo.placa = document.getElementById('txtPlaca').value.toUpperCase();
    Veiculo.cor = document.getElementById('txtCor').value.toUpperCase();
    Veiculo.ativo = true;
    JsonVeiculo = JSON.stringify(Veiculo);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/veiculo', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        request.send(JsonVeiculo);        
        request.onreadystatechange = function(){
            if(request.readyState == request.DONE){
                if(request.status == 200){
                    var NovoVeiculo = new Object();
                    NovoVeiculo = JSON.parse(request.response);                    
                    if(NovoVeiculo.status != 0){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Novo veículo registrado com sucesso!";
                        //Esse inserido por fora funciona da seguinte maneira
                        //Caso o veículo esteja sendo inserido durante um cadastro
                        //Ele deve enviar true, com isso, quando o cadastro estiver finalizado
                        //Ele fechara o modal de cadastro de veículo e atualizará a lista
                        //Caso seja false, ele ira atualizar a pagina completamente
                        if(inserindoPorFora){
                            document.getElementById("slcVeiculo").innerHTML = "";
                            listaVeiculos();
                            setTimeout(function(){
                                $('#modalVeiculos').modal('hide');
                            },2000);
                        }else{
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        }
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(NovoVeiculo[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "O veículo informado já esta cadastrado(Placa repetida)!";
                        }
                        if(NovoVeiculo[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo veículo\nDescrição do erro.: '+err);
    }
}

function listaVeiculos(listaTabela){
    if(listaTabela){
        var corpoTabelaVeiculo = document.getElementById('corpoTabela');
        corpoTabelaVeiculo.innerHTML = "";
    }else{        
        document.getElementById("slcVeiculo").innerHTML = "";
        var selectVeiculos = document.getElementById('slcVeiculo');
    }
    request = new XMLHttpRequest();
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('GET',url+':8090/api/veiculo', true);
    request.setRequestHeader("Authorization", "bearer "+token);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == XMLHttpRequest.DONE){
            if(request.status == 200){
                var Veiculos = new Object();                
                Veiculos = JSON.parse(request.response);
                if(!listaTabela){
                    for(var veiculo in Veiculos){
                        //Insere ID Usuario na tabela
                        var option = document.createElement('option');
                        var texto = document.createTextNode(Veiculos[veiculo].placa+ ' - ' +Veiculos[veiculo].modelo + ' - '+ Veiculos[veiculo].cor);
                        option.value = Veiculos[veiculo].codveiculo;
                        option.appendChild(texto);
                        selectVeiculos.appendChild(option);
                    }
                }else{
                    for(var veiculo in Veiculos){
                        var linha = document.createElement("tr");
                        var codVeiculo = document.createElement("td");
                        var texto = document.createTextNode(Veiculos[veiculo].codveiculo);
                        codVeiculo.appendChild(texto);

                        var modelo = document.createElement("td");
                        var texto = document.createTextNode(Veiculos[veiculo].modelo);
                        modelo.appendChild(texto);

                        var placa = document.createElement("td");
                        var texto = document.createTextNode(Veiculos[veiculo].placa);
                        placa.appendChild(texto);

                        var cor = document.createElement("td");
                        var texto = document.createTextNode(Veiculos[veiculo].cor);
                        cor.appendChild(texto);

                        var spanIconAlterar = document.createElement("span");
                        var btnAlterar = document.createElement("button");
                        btnAlterar.className = "btn btn-info m-0";
                        btnAlterar.id = 'btnAlterar'+veiculo;
                        spanIconAlterar.className = "fa fa-edit";
                        btnAlterar.appendChild(spanIconAlterar);
                        btnAlterar.title = "Alterar veiculo";
                        btnAlterar.setAttribute("data-toggle","modal");
                        btnAlterar.setAttribute("data-target","#modalVeiculo");
                        btnAlterar.setAttribute("role","button");
                        btnAlterar.setAttribute("role","dialog");
                        btnAlterar.codveiculo = Veiculos[veiculo].codveiculo;
                        btnAlterar.modelo = Veiculos[veiculo].modelo;
                        btnAlterar.placa = Veiculos[veiculo].placa;
                        btnAlterar.cor =  Veiculos[veiculo].cor;
    
                        btnAlterar.onclick = function(){
                            var btnSalvar = document.getElementById('btnSalvar');
                            var tituloModal = document.getElementById('tituloModal');
                            var txtModelo = document.getElementById('txtModelo');
                            var txtPlaca = document.getElementById('txtPlaca');
                            var txtCor = document.getElementById('txtCor');
                            txtModelo.value = this.modelo;
                            txtPlaca.value = this.placa;
                            txtCor.value = this.cor;
                            tituloModal.innerHTML = 'Alterar Veiculo';
                            btnSalvar.innerHTML = '';
                            btnSalvar.innerHTML = '<img src="img/icones/SalvarEFechar/icons8-salvar-e-fechar-36.png"> Salvar';
                            btnSalvar.codveiculo = this.codveiculo;
                            btnSalvar.onclick = function(){
                                alteraVeiculo(this.codveiculo);
                            }
                        }
                        /*
                        *   Cria dinamicamente botão de desativar com suas propriedas na tabela
                        */                    
    
                        var spanIconDesativar = document.createElement("span");
                        var btnDesativar = document.createElement("button");
                        btnDesativar.className = "btn btn-danger ml-1 ";
                        btnDesativar.id = "btnDesativar"+veiculo;
                        spanIconDesativar.className = "fa fa-trash";
                        btnDesativar.appendChild(spanIconDesativar);
                        btnDesativar.title = "Desativar veiculo";
                        btnDesativar.codveiculo = Veiculos[veiculo].codveiculo;
                        btnDesativar.setAttribute("data-toggle","modal");
                        btnDesativar.setAttribute("data-target","#modalDesativacaoVeiculo");
                        btnDesativar.setAttribute("role","button");
                        btnDesativar.setAttribute("role","dialog");
                        var btnDesativaVeiculoModal = document.getElementById('btnDesativaVeiculo');
                        btnDesativar.onclick = function(){
                            btnDesativaVeiculoModal.codveiculo = this.codveiculo;
                        }
                        var tdAcoes = document.createElement("td");
                        tdAcoes.appendChild(btnAlterar);
                        tdAcoes.appendChild(btnDesativar);
                        tdAcoes.className = "largura-acoes";
                        linha.appendChild(codVeiculo);
                        linha.appendChild(modelo);
                        linha.appendChild(placa);
                        linha.appendChild(cor);
                        linha.appendChild(tdAcoes);
                        corpoTabelaVeiculo.appendChild(linha);
                    }
                }
                
            }
        }
    }
    
}
function desativaVeiculo(codVeiculo){
    var Veiculo = new Object();
    var request = new XMLHttpRequest();
    var divErro = document.getElementById("divErroModalDesativacao");
    var divTextoErro = document.getElementById("divTextoErroModalDesativacao");
    var jsonVeiculo;
    Veiculo.codveiculo = codVeiculo;
    Veiculo.ativo = false;
    jsonVeiculo = JSON.stringify(Veiculo);
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('PUT', url+':8090/api/veiculo/desativa/', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{        
        request.responseType = "text";
        request.send(jsonVeiculo);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var VeiculoDesativado = new Object();
                    VeiculoDesativado = JSON.parse(request.response);
                    if(VeiculoDesativado.status != false){
                        divErro.className = "d-flex col-12 justify-content-center ";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-success text-center" ;
                        divTextoErro.innerHTML = "Veículo desativado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },3500);
                    }else{                        
                        divErro.className = "d-flex col-12 justify-content-center ";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center" ;
                        divTextoErro.innerHTML = "Não foi possivel desativar o veículo selecionado<br>Por favor verifique o console do navegador'";
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao desativar veículo\nDescrição do erro.: '+err);
    }
}

function alteraVeiculo(codveiculo){
    var Veiculo = new Object();
    var request = new XMLHttpRequest();    
    var divErro = document.getElementById("divErroVeiculo");
    var divTextoErro = document.getElementById("divTextoErroVeiculo");
    var jsonVeiculo;
    Veiculo.modelo = document.getElementById('txtModelo').value;
    Veiculo.placa = document.getElementById('txtPlaca').value;
    Veiculo.cor = document.getElementById('txtCor').value;    
    Veiculo.codveiculo = codveiculo;
    if(document.getElementById('txtModelo').value == null || document.getElementById('txtModelo').value == ""){
        Veiculo.modelo = undefined;
    }    
    if(document.getElementById('txtPlaca').value == null || document.getElementById('txtPlaca').value == ""){
        Veiculo.placa = undefined;
    }
    if(document.getElementById('txtCor').value == null || document.getElementById('txtCor').value == ""){
        Veiculo.cor = undefined;
    }
    jsonVeiculo = JSON.stringify(Veiculo);    
    var token = document.cookie;
    token = token.replace("tk=", "");
    request.open('POST', url+':8090/api/veiculo/altera/', true);
    request.setRequestHeader("Content-Type", "Application/json");
    request.setRequestHeader("Authorization", "bearer "+token);
    try{
        request.send(jsonVeiculo);
        request.onreadystatechange = function(){
            if(request.readyState = request.DONE){
                if(request.status == 200){
                    var veiculoAlterador = new Object();
                    veiculoAlterador = JSON.parse(request.response);
                    if(veiculoAlterador.status != false){
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 col-md-12 col-xl-12 col-lg-12 alert alert-success text-center";
                        divTextoErro.innerHTML = "Veículo alterado com sucesso!";
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }else{
                        divErro.className = "d-flex col-12 justify-content-center";
                        divTextoErro.className = "col-6 col-sm-12 alert alert-danger text-center";
                        if(veiculoAlterador[0].errorInfo[0] == 23505){
                            divTextoErro.innerHTML = "O veículo informado já esta cadastrado!";
                        }
                        if(veiculoAlterador[0].errorInfo[0] == 23502){
                            divTextoErro.innerHTML = "Por favor, preencha todos os campos";
                        }
                    }
                }
            }
        }
    }catch(err){
        console.log('Erro ao cadastrar novo usuario\nDescrição do erro.: '+err);
    }
}

function PreparaFormCadastro(){
    var btnSalvar = document.getElementById('btnSalvar');
    var tituloModal = document.getElementById('tituloModal');
    document.getElementById("formVeiculo").reset();
    tituloModal.innerHTML = 'Novo Veículo';
    btnSalvar.innerHTML = '';
    btnSalvar.innerHTML = '<img src="img/icones/SalvarEFechar/icons8-salvar-e-fechar-36.png"> Salvar';
}